import sys, os, time, datetime, re, platform
from PyQt5 import QtCore as qtc
from PyQt5 import QtWidgets as qtw
from PyQt5 import QtGui as qtg
from PyQt5 import QtMultimedia as qtmm
from PyQt5 import QtMultimediaWidgets as qtmmw
import serial
from serial.tools.list_ports import comports
from math import sin, cos, atan2, degrees, sqrt
import random

class TimedScanWindow(qtw.QWidget):             

    applied = qtc.pyqtSignal()
    abort = qtc.pyqtSignal()

    def __init__(self, parent, applied=None):                    
        super().__init__()
        self.parent = parent
        # handle exit condition:
        parent.destroyed.connect(self.close)
        if applied:
            self.applied.connect(applied)
        self.setFixedSize(qtc.QSize(500,250))
        self.setWindowTitle('SMoCCscan Timer')
        layout = qtw.QVBoxLayout()
        self.setLayout(layout)
        self.heading = qtw.QLabel('Timed Scans')
        self.heading.setFont(qtg.QFont('Arial', 12))
        self.heading.setAlignment(qtc.Qt.AlignCenter | qtc.Qt.AlignVCenter)
        self.heading.setFixedHeight(40)
        buttonSize = qtc.QSize(110,35)
        self.clock = qtw.QLCDNumber()
        self.clock.setDigitCount(8)
        self.clock.display('0:00:00')
        self.buttonbox = qtw.QWidget()
        self.buttonbox.setFixedHeight(60)
        self.minutes = qtw.QSpinBox(
            self,
            maximum=1440,
            minimum=1,
            suffix=' min',
            singleStep=1
            )
        self.minutes.setFixedSize(buttonSize)
        try:
            self.minutes.setValue(self.parent.interval_minutes)
        except:
            self.minutes.setValue(120)
        self.applyButton = qtw.QPushButton('Run', clicked=self.on_apply)
        self.pauseButton = qtw.QPushButton('Pause', clicked=self.on_pause)
        self.cancelButton = qtw.QPushButton('Stop&&Exit', clicked=self.on_close)
        self.applyButton.setDefault(True)
        self.pauseButton.setEnabled(False)
        self.cancelButton.setFixedSize(buttonSize)
        self.pauseButton.setFixedSize(buttonSize)
        self.applyButton.setFixedSize(buttonSize)
        buttonlayout = qtw.QHBoxLayout()
        self.buttonbox.setLayout(buttonlayout)
        buttonlayout.setAlignment(qtc.Qt.AlignRight)
        buttonlayout.addWidget(self.minutes)
        buttonlayout.addWidget(self.applyButton)
        buttonlayout.addWidget(self.pauseButton)
        buttonlayout.addWidget(self.cancelButton)
        # add all the widgets to layout:
        layout.addWidget(self.heading)
        layout.addWidget(self.clock)
        layout.addWidget(self.buttonbox)
        # End main UI code
        self.show()
        
    def keyPressEvent(self, qKeyEvent):
        if qKeyEvent.key() == qtc.Qt.Key_Return: 
            self.on_apply()
        else:
            super().keyPressEvent(qKeyEvent)

    def on_apply(self):
        self.parent.interval_minutes = self.minutes.value()
        self.parent.setSettings()
        self.minutes.setEnabled(False)
        self.pauseButton.setText('Pause')
        self.pauseButton.setEnabled(True)
        self.heading.setText('Active, next scan in:')
        interval = self.parent.interval_minutes * 60 * 1000
        self.timer = qtc.QTimer()
        self.timer.setInterval(interval)
        self.timer.timeout.connect(self.on_trigger)
        self.timer.start()
        #timer for display update:
        self.timer2 = qtc.QTimer()
        self.timer2.setInterval(200)
        self.timer2.timeout.connect(self.update_clock)
        self.timer2.start()
        
    def update_clock(self):
        if self.timer.isActive():
            time_left = (self.timer.remainingTime() // 1000) + 1
            time_display = str(datetime.timedelta(seconds=time_left))
            self.clock.display(time_display)
        else:
            pass

    def on_pause(self):
        if self.timer.isActive():
            self.time_left = self.timer.remainingTime()
            self.pauseButton.setText('Resume')
            self.minutes.setEnabled(True)
            self.heading.setText('Paused')
            self.timer.stop()
        else:
            self.parent.interval_minutes = self.minutes.value()
            self.parent.setSettings()
            self.pauseButton.setText('Pause')
            self.minutes.setEnabled(False)
            self.heading.setText('Active, next scan in:')
            self.timer.start(self.time_left)

    def on_trigger(self):
        self.clock.display('0:00:00')
        self.heading.setText('Scan in progress, please wait...')
        self.timer.stop()
        self.parent.fullscan()
        self.parent.scan.finished.connect(self.restart_after_scan)

    def restart_after_scan(self):
        # reset interval, in case pause was pressed
        try:
            self.heading.setText('Active, next scan in:')
            interval = self.parent.interval_minutes * 60 * 1000
            self.timer.setInterval(interval)
            self.timer.start()
        except:
            print("Error restarting timer.")
        
    def on_close(self):
        self.abort.emit()
        self.minutes.setEnabled(True)
        self.pauseButton.setEnabled(False)
        try:
            self.timer.stop()
        except:
            pass
        try:
            self.timer2.stop()
        except:
            pass
        self.close()

class ColorButton(qtw.QPushButton):

    changed = qtc.pyqtSignal()

    def __init__(self, default_color, changed=None):
        super().__init__()
        self.setFixedSize(qtc.QSize(240,25))
        self.set_color(qtg.QColor(default_color))
        self.clicked.connect(self.on_click)
        if changed:
            self.changed.connect(changed)

    def set_color(self, color):
        self._color = color
        # update icon
        pixmap = qtg.QPixmap(234, 19)
        pixmap.fill(self._color)
        self.setIcon(qtg.QIcon(pixmap))
        self.setIconSize(qtc.QSize(234,19))

    def on_click(self):
        color = qtw.QColorDialog.getColor(self._color)
        if color:
            self.set_color(color)
            self.changed.emit()

class FontButton(qtw.QPushButton):

    changed = qtc.pyqtSignal()

    def __init__(self, default_family, default_pointsize, changed=None):
        super().__init__()
        self.set_font(qtg.QFont(default_family, default_pointsize))
        self.clicked.connect(self.on_click)
        self.setFixedSize(qtc.QSize(240,35))
        if changed:
            self.changed.connect(changed)

    def set_font(self, font):
        self._font = font
        displayfont = qtg.QFont(font.family(), int(font.pointSize()/2))
        self.setFont(displayfont)
        self.setText(f'{font.family()} {font.pointSize()}')

    def on_click(self):
        font, accepted = qtw.QFontDialog.getFont(self._font)
        if accepted:
            self.set_font(font)
            self.changed.emit()

class mouseableLabel(qtw.QLabel):
    clicked_left = qtc.pyqtSignal()
    clicked_right = qtc.pyqtSignal()
    def mouseReleaseEvent(self, QMouseEvent):
        if QMouseEvent.button() == qtc.Qt.LeftButton:
            self.clicked_left.emit()
        elif QMouseEvent.button() == qtc.Qt.RightButton:
            self.clicked_right.emit()
            

class AsSettingsWindow(qtw.QWidget):             

    applied = qtc.pyqtSignal()

    def __init__(self, parent, applied=None):                    
        super().__init__()
        # handle exit condition:
        self.parent = parent
        parent.destroyed.connect(self.close)
        # recover settings:
        if applied:
            self.applied.connect(applied)
        self.setFixedSize(qtc.QSize(650,1100))
        self.setWindowTitle('SMoCCscan Advanced Settings')
        buttonSize = qtc.QSize(110,35)
        layout = qtw.QVBoxLayout()
        cam_widget = qtw.QWidget()
        layout_h = qtw.QHBoxLayout()
        self.setLayout(layout)
        cam_widget.setLayout(layout_h)
        cam_widget.setFixedHeight(210)
        # Group boxes:
        main_groupbox = qtw.QGroupBox(
            'Main Cam',
            alignment=qtc.Qt.AlignHCenter
            )
        aux_groupbox = qtw.QGroupBox(
            'Aux Cam',
            alignment=qtc.Qt.AlignHCenter
            )
        machine_groupbox = qtw.QGroupBox(
            'Machine Settings',
            alignment=qtc.Qt.AlignHCenter
            )
        general_groupbox = qtw.QGroupBox(
            'Visual Settings',
            alignment=qtc.Qt.AlignHCenter
            )
        text_groupbox = qtw.QGroupBox(
            alignment=qtc.Qt.AlignHCenter
            )
        button_groupbox = qtw.QGroupBox(
            'Reprocessing (with current settings)',
            alignment=qtc.Qt.AlignHCenter
            )
        text_groupbox.setStyleSheet('border:none;margin: 10px 0px 0px 0px;padding: 0px 0px 0px 0px')
        main_groupbox.setSizePolicy(qtw.QSizePolicy.Minimum,qtw.QSizePolicy.Minimum)
        aux_groupbox.setSizePolicy(qtw.QSizePolicy.Minimum,qtw.QSizePolicy.Minimum)
        general_groupbox.setSizePolicy(qtw.QSizePolicy.Minimum,qtw.QSizePolicy.Minimum)
        machine_groupbox.setSizePolicy(qtw.QSizePolicy.Minimum,qtw.QSizePolicy.Minimum)
        button_groupbox.setSizePolicy(qtw.QSizePolicy.Minimum,qtw.QSizePolicy.Minimum)
        text_groupbox.setSizePolicy(qtw.QSizePolicy.Minimum,qtw.QSizePolicy.Minimum)
        main_groupbox.setLayout(qtw.QFormLayout())
        aux_groupbox.setLayout(qtw.QFormLayout())
        general_groupbox.setLayout(qtw.QFormLayout())
        machine_groupbox.setLayout(qtw.QFormLayout())
        text_groupbox.setLayout(qtw.QFormLayout())
        button_groupbox.setLayout(qtw.QHBoxLayout())
        self.default_anno = qtw.QCheckBox()
        self.custom_anno = qtw.QCheckBox()
        self.mach_homing = qtw.QCheckBox()
        self.mach_initialpos = qtw.QCheckBox()
        self.custom_text = qtw.QLineEdit()
        self.custom_text.setMaxLength(27)
        self.custom_text.setFixedWidth(240)
        self.showgrid = qtw.QCheckBox()
        self.gridcol = ColorButton((qtg.QColor('white')))
        self.bordercol = ColorButton((qtg.QColor('white')))
        self.vialcol = ColorButton((qtg.QColor('white')))
        self.annocol = ColorButton((qtg.QColor('white')))
        self.textFont = FontButton(qtg.QFont("Helvetica", 10).family(), qtg.QFont("Helvetica", 10).pointSize())
        self.main_resize = qtw.QComboBox()
        self.main_resize.addItems([str(1.00), str(0.75), str(0.50), str(0.25), str(0.10)])
        self.main_refpix = qtw.QSpinBox(
            self,
            value=35,
            maximum=2048,
            minimum=1,
            suffix=' pix',
            singleStep=1
            )
        self.main_refmm = qtw.QDoubleSpinBox(
            self,
            value=0.5,
            maximum=10,
            minimum=0.01,
            suffix=' mm',
            singleStep=0.05
            )
        self.main_spacing = qtw.QDoubleSpinBox(
            self,
            value=0.2,
            maximum=1,
            minimum=0.01,
            suffix=' mm',
            singleStep=0.05
            )  
        self.aux_resize = qtw.QComboBox()
        self.aux_resize.addItems([str(1.00), str(0.75), str(0.50), str(0.25), str(0.10)])
        self.aux_refpix = qtw.QSpinBox(
            self,
            value=35,
            maximum=2048,
            minimum=1,
            suffix=' pix',
            singleStep=1
            )
        self.aux_refmm = qtw.QDoubleSpinBox(
            self,
            value=0.5,
            maximum=10,
            minimum=0.01,
            suffix=' mm',
            singleStep=0.05
            )
        self.aux_spacing = qtw.QDoubleSpinBox(
            self,
            value=0.2,
            maximum=1,
            minimum=0.01,
            suffix=' mm',
            singleStep=0.05
            )    
        self.initial_x = qtw.QDoubleSpinBox(
            self,
            value=0,
            maximum=300,
            minimum=0,
            suffix=' mm',
            singleStep=1
            )
        self.initial_y = qtw.QDoubleSpinBox(
            self,
            value=0,
            maximum=300,
            minimum=0,
            suffix=' mm',
            singleStep=1
            )
        self.mach_drivespeed = qtw.QSpinBox(
            self,
            value=1000,
            maximum=5000,
            minimum=100,
            singleStep=100
            )
        self.mach_drivespeed.setMaximumWidth(120)
        self.mach_drivespeed.setValue(1000)
        self.initial_x.setMaximumWidth(120)
        self.initial_y.setMaximumWidth(120)
        self.initial_layout = qtw.QHBoxLayout()
        self.initial_layout.addWidget(self.initial_x)
        self.initial_layout.addWidget(self.initial_y)
        text_groupbox.layout().addRow('Image resize factor: ', None)
        text_groupbox.layout().addRow('Reference length (on unscaled image): ', None)
        text_groupbox.layout().addRow('Reference length (known): ', None)
        text_groupbox.layout().addRow('Grid spacing on image: ', None)
        general_groupbox.layout().addRow('Annotate Date/Time/Cam: ', self.default_anno)
        general_groupbox.layout().addRow('Annotate Notes: ', self.custom_anno)
        general_groupbox.layout().addRow('Notes Text: ', self.custom_text)
        general_groupbox.layout().addRow('Show internal Grid: ', self.showgrid)
        general_groupbox.layout().addRow('Internal Grid Color: ', self.gridcol)
        general_groupbox.layout().addRow('Border Color: ', self.bordercol)
        general_groupbox.layout().addRow('Vial Label Text Color: ', self.vialcol)
        general_groupbox.layout().addRow('Annotation Text Color: ', self.annocol)
        general_groupbox.layout().addRow('Annotation && Label Font: ', self.textFont)
        homingtext = qtw.QLabel()
        homingtext_string = '''
        Homing requires physical endstops and correctly configured firmware.
        Ensure that firmware max X/Y travel does not exceed physical machine limits!
        Homing will run automatically on connect, then drive to initital X/Y position.
        MAKE SURE PATHS ARE CLEAR!'''
        homingtext.setWordWrap(True)
        homingtext.setStyleSheet('border:none;margin: 0px 0px 0px 0px;padding: 0px 0px 0px 0px')
        homingtext.setAlignment(qtc.Qt.AlignLeft | qtc.Qt.AlignVCenter)
        homingtext.setFixedHeight(100)
        homingtext.setText(homingtext_string)
        machine_groupbox.layout().addRow('Enable Homing: ', self.mach_homing)
        machine_groupbox.layout().addRow('Drive to Initial Position: ', self.mach_initialpos)
        machine_groupbox.layout().addRow('Initial Position x/y: ', self.initial_layout)
        machine_groupbox.layout().addRow('Drive Speed (except homing, 1000 is safe): ', self.mach_drivespeed)
        machine_groupbox.layout().addRow(homingtext)
        main_groupbox.layout().addRow(None, self.main_resize)
        main_groupbox.layout().addRow(None, self.main_refpix)
        main_groupbox.layout().addRow(None, self.main_refmm)
        main_groupbox.layout().addRow(None, self.main_spacing)
        aux_groupbox.layout().addRow(None, self.aux_resize)
        aux_groupbox.layout().addRow(None, self.aux_refpix)
        aux_groupbox.layout().addRow(None, self.aux_refmm)
        aux_groupbox.layout().addRow(None, self.aux_spacing)
        # reprocess buttons:
        self.reprocessButton = qtw.QPushButton('Reprocess (Stitch)')
        self.reprocessButton.clicked.connect(self.on_reprocess)        
        self.reprocessButton.setFixedSize(buttonSize)
        self.reprocessButton.setFixedWidth(150)
        self.analyzeButton = qtw.QPushButton('Reprocess (Analyze)')
        self.analyzeButton.clicked.connect(self.on_reanalyze)        
        self.analyzeButton.setFixedSize(buttonSize)
        self.analyzeButton.setFixedWidth(150)
        button_groupbox.layout().addWidget(self.reprocessButton)
        button_groupbox.layout().addWidget(self.analyzeButton)
        # action button box:
        self.buttonbox = qtw.QWidget()
        self.buttonbox.setFixedHeight(60)
        self.cancelButton = qtw.QPushButton('Cancel', clicked=self.on_close)
        self.applyButton = qtw.QPushButton('OK', clicked=self.on_apply)
        self.applyButton.setDefault(True)
        self.cancelButton.setFixedSize(buttonSize)
        self.applyButton.setFixedSize(buttonSize)
        buttonlayout = qtw.QHBoxLayout()
        self.buttonbox.setLayout(buttonlayout)
        buttonlayout.setAlignment(qtc.Qt.AlignRight)
        buttonlayout.addWidget(self.cancelButton)
        buttonlayout.addWidget(self.applyButton)
        instructions = qtw.QLabel()
        instruction_string = '''
        Reference lengths are used for grid spacing calculation: Take a photo of an
        object with a known dimension (such as a 0.4 mm steel ball). Measure the
        number of pixels on a snapshot of the object and adjust both values.'''
        instructions.setWordWrap(True)
        instructions.setStyleSheet('border:none;margin: 0px 0px 0px 0px;padding: 0px 0px 0px 0px')
        instructions.setAlignment(qtc.Qt.AlignLeft | qtc.Qt.AlignVCenter)
        instructions.setFixedHeight(80)
        instructions.setText(instruction_string)
        # add all the widgets to layout:
        layout.addWidget(machine_groupbox)
        layout.addWidget(general_groupbox)
        layout_h.addWidget(text_groupbox)
        layout_h.addWidget(main_groupbox)
        layout_h.addWidget(aux_groupbox)
        layout.addWidget(cam_widget)
        layout.addWidget(instructions)
        layout.addWidget(button_groupbox)
        layout.addStretch()
        layout.addWidget(self.buttonbox)
        # get settings and end main UI code
        self.get_settings()
        self.show()
        
    def keyPressEvent(self, qKeyEvent):
        if qKeyEvent.key() == qtc.Qt.Key_Return: 
            self.on_apply()
        else:
            super().keyPressEvent(qKeyEvent)  

    def on_reprocess(self):
        self.save_settings()
        self.applied.emit()
        self.parent.merge_reprocess()
        self.close()

    def on_reanalyze(self):
        self.save_settings()
        self.applied.emit()
        self.parent.analyze_reprocess()
        self.close()

    def on_apply(self):
        self.save_settings()
        self.applied.emit()
        self.close()

    def save_settings(self):
        self.parent.settings.setValue('MA_homing', self.mach_homing.isChecked())
        self.parent.settings.setValue('MA_initialpos', self.mach_initialpos.isChecked())
        self.parent.settings.setValue('MA_initial_x', self.initial_x.value())
        self.parent.settings.setValue('MA_initial_y', self.initial_y.value())
        self.parent.settings.setValue('MA_drivespeed', self.mach_drivespeed.value())
        self.parent.settings.setValue('AS_defaultanno', self.default_anno.isChecked())
        self.parent.settings.setValue('AS_customanno', self.custom_anno.isChecked())
        self.parent.settings.setValue('AS_showgrid', self.showgrid.isChecked())
        self.parent.settings.setValue('AS_notes', self.custom_text.text())
        self.parent.settings.setValue('AS_gridcolor', self.gridcol._color)
        self.parent.settings.setValue('AS_bordercolor', self.bordercol._color)
        self.parent.settings.setValue('AS_vialcolor', self.vialcol._color)
        self.parent.settings.setValue('AS_annotationcolor', self.annocol._color)
        self.parent.settings.setValue('AS_font', self.textFont._font.toString())  
        self.parent.settings.setValue('AS_main_resizefactor', self.main_resize.currentText())
        self.parent.settings.setValue('AS_aux_resizefactor', self.aux_resize.currentText())
        self.parent.settings.setValue('AS_main_refpix', self.main_refpix.value())
        self.parent.settings.setValue('AS_aux_refpix', self.aux_refpix.value())
        self.parent.settings.setValue('AS_main_reflength', self.main_refmm.value())
        self.parent.settings.setValue('AS_aux_reflength', self.aux_refmm.value())
        self.parent.settings.setValue('AS_main_spacing', self.main_spacing.value())
        self.parent.settings.setValue('AS_aux_spacing', self.aux_spacing.value())

    def get_settings(self):
        self.mach_homing.setChecked(self.parent.settings.value('MA_homing', False, type=bool))
        self.mach_initialpos.setChecked(self.parent.settings.value('MA_initialpos', False, type=bool))
        self.initial_x.setValue(float(self.parent.settings.value('MA_initial_x', 0)))
        self.initial_y.setValue(float(self.parent.settings.value('MA_initial_y', 0)))
        self.mach_drivespeed.setValue(int(self.parent.settings.value('MA_drivespeed', 1000)))
        self.default_anno.setChecked(self.parent.settings.value('AS_defaultanno', True, type=bool))  
        self.custom_anno.setChecked(self.parent.settings.value('AS_customanno', False, type=bool))
        self.showgrid.setChecked(self.parent.settings.value('AS_showgrid', False, type=bool))
        self.custom_text.setText(self.parent.settings.value('AS_notes', 'my Notes'))
        self.gridcol.set_color(self.parent.settings.value('AS_gridcolor', qtg.QColor('#00FF00')))
        self.bordercol.set_color(self.parent.settings.value('AS_bordercolor', qtg.QColor('#FFFFFF')))
        self.vialcol.set_color(self.parent.settings.value('AS_vialcolor', qtg.QColor('#FFFFFF')))
        self.annocol.set_color(self.parent.settings.value('AS_annotationcolor', qtg.QColor('#FFFF00')))
        fnt = qtg.QFont()
        fnt.fromString(self.parent.settings.value('AS_font', qtg.QFont("Helvetica", 10).toString()))
        self.textFont.set_font(fnt)
        mind = self.main_resize.findText(self.parent.settings.value('AS_main_resizefactor', '0.5')) 
        aind = self.aux_resize.findText(self.parent.settings.value('AS_aux_resizefactor', '0.5'))
        if mind != -1: # -1 for not found
            self.main_resize.setCurrentIndex(mind)
        if aind != -1: # -1 for not found
            self.aux_resize.setCurrentIndex(aind)
        self.main_refpix.setValue(int(self.parent.settings.value('AS_main_refpix', 35)))
        self.aux_refpix.setValue(int(self.parent.settings.value('AS_aux_refpix', 35)))
        self.main_refmm.setValue(float(self.parent.settings.value('AS_main_reflength', 0.4)))
        self.aux_refmm.setValue(float(self.parent.settings.value('AS_aux_reflength', 0.4)))
        self.main_spacing.setValue(float(self.parent.settings.value('AS_main_spacing', 0.2)))
        self.aux_spacing.setValue(float(self.parent.settings.value('AS_aux_spacing', 0.2)))
        

    def on_close(self):
        self.close()

class FullScan(qtc.QObject):
    finished = qtc.pyqtSignal(str,str)
    snapshot = qtc.pyqtSignal(str, str)

    def __init__(self, parent):
        super().__init__()
        self.parent = parent
        self.direction = 'positive'
        self.is_aux_run = False
        # iterate every time the camera reports ready:
        self.xcount = self.parent.noX.value()-1
        self.ycount = self.parent.noY.value()-1
        self.xcurrent = 0
        self.ycurrent = 0
        if self.parent.altcamselect.currentIndex() != 0:
            self.aux = True
            self.aux_x = self.parent.altcam_offx.value()
            self.aux_y = self.parent.altcam_offy.value()
        else:
            self.aux = False
        self.done = False
        self.xmov = self.parent.dX.value()
        self.ymov = self.parent.dY.value()
        # calculate move increments (from rotation angle) & pauses (3 second minimum):
        if self.parent.orientselect.currentIndex() == 0:
            self.pause_short = max(int(self.xmov/10), 3) * 1000         
        else:
            self.col_increment_x = cos(self.parent.rotation) * self.parent.dX.value()
            self.col_increment_y = sin(self.parent.rotation) * self.parent.dX.value()
            # increments for the move between rows (tested for both cw and ccw):
            self.row_increment_x = -sin(self.parent.rotation) * self.parent.dY.value()
            self.row_increment_y =  cos(self.parent.rotation) * self.parent.dY.value()
            dist = sqrt(self.col_increment_x**2+self.col_increment_y**2)
            self.pause_short = max(int(dist/10), 3) * 1000
        self.parent.goto_origin()       # start at A1
        if self.parent.activecam == 'AUX':     # correct starting position for AUX offset if AUX cam is active
            self.parent.drive_relative(self.aux_x,self.aux_y)
        # activate stop button
        self.parent.stop_btn.setEnabled(True)
        self.parent.stop_btn.setVisible(True)
        # prepare directory:
        self.timestamp = time.strftime('%d%b%Y-%H_%M_%S')
        self.parent.save_path.mkdir('scan-%s' % (self.timestamp))
        self.path = self.parent.save_path.absolutePath()
        # connect break signal:
        self.parent.emergency_stop.connect(self.finish)
        # connect to camera
        if self.aux == True:
            self.parent.altcapture.imageSaved.connect(self.run)
        self.parent.capture.imageSaved.connect(self.run)
        # disable aux/main camera swap functionality while scan runs:
        self.parent.notebook.tabBarClicked.disconnect(self.parent.handle_tabbar_clicked)
        self.parent.notebook.tabBarClicked.connect(self.parent.handle_tabbar_clicked_nomoves)
        
    def run(self):
        """"changed from line scanning to snake-like move forward -> apply aux offset once ->
        move backward -> back to start position        
        """
        if self.done:
            self.finish()
        else:
            # fotos first, then move (simpler logic):
            self.vial = chr(self.ycurrent+65) + str(self.xcurrent+1)
            # prep file name here:
            if self.is_aux_run == False:
                file = os.path.join(self.path,
                    'scan-%s' % (self.timestamp),
                    'main-%s.jpg' % (self.vial)
                    )
            else:
                file = os.path.join(self.path,
                    'scan-%s' % (self.timestamp),
                    'aux-%s.jpg' % (self.vial)
                    ) 
            # reports to main window status bar:
            if not self.parent.Simulation_Mode:
                if self.is_aux_run == False:
                    self.parent.status.showMessage('SCAN (Main cam): vial ' + self.vial + ' done.')
                else:
                    self.parent.status.showMessage('SCAN (Aux cam): vial ' + self.vial + ' done.')
            else:
                if self.is_aux_run == False:
                    self.parent.status.showMessage('SCAN (Main cam): vial ' + self.vial + ' done. - SIMULATION MODE')
                else:
                    self.parent.status.showMessage('SCAN (Aux cam): vial ' + self.vial + ' done. - SIMULATION MODE')
            # min 3 seconds delay to stabilize before/after shot, use QEventloop to make it non-blocking:
            loop = qtc.QEventLoop()
            qtc.QTimer.singleShot(self.pause_short, loop.quit)
            loop.exec_()
            if self.is_aux_run == False:
                self.snapshot.emit(file, 'main')
            else:
                self.snapshot.emit(file, 'aux')
            ### DRIVE LOGIC STARTS HERE
            # last cell in a line when moving right (shift y by one row and reverse x direction):
            if self.xcurrent == self.xcount and self.direction == 'positive':
                # is this the final vial?
                # yes, and aux is not present (trigger exit):
                if self.ycurrent == self.ycount and self.aux == False:
                    self.done = True
                # yes, and aux is present (move once by aux offset, reverse direction & change scan type to aux):
                elif self.ycurrent == self.ycount and self.aux == True and self.is_aux_run == False:
                    self.parent.status.showMessage('SCAN: switching to AUX')
                    if not self.parent.Simulation_Mode:
                        self.parent.drive_relative(-self.aux_x , -self.aux_y)
                    self.is_aux_run = True
                    self.direction = 'negative'
                # yes, and aux run is completed (trigger exit):
                elif self.ycurrent == 0 and self.is_aux_run == True:
                    self.done = True
                # not final vial (move y, reverse direction). 
                else: 
                    # when oriented parallel to axes:
                    if self.parent.orientselect.currentIndex() == 0:
                        if self.is_aux_run == False:
                            self.parent.drive_relative(0, self.ymov)
                            self.ycurrent += 1
                        else:
                            self.parent.drive_relative(0, -self.ymov)
                            self.ycurrent -= 1
                    # when oriented by 1st/last vial in row:
                    else:
                        if self.is_aux_run == False:
                            self.parent.drive_relative(self.row_increment_x, self.row_increment_y)
                            self.ycurrent += 1
                        else:
                            self.parent.drive_relative(-self.row_increment_x, -self.row_increment_y)
                            self.ycurrent -= 1  
                    self.direction = 'negative'
            # first cell in a line when moving left (shift y by one row and reverse x direction):
            elif self.xcurrent == 0 and self.direction == 'negative':
                # is this the final vial?
                # yes, and aux is not present (trigger exit):
                if self.ycurrent == self.ycount and self.aux == False:
                    self.done = True
                # yes, and aux is present (move once by aux offset, reverse direction & change scan type to aux):
                elif self.ycurrent == self.ycount and self.aux == True and self.is_aux_run == False:
                    self.parent.status.showMessage('SCAN: switching to AUX')
                    if not self.parent.Simulation_Mode:
                        self.parent.drive_relative(-self.aux_x , -self.aux_y)
                    self.is_aux_run = True
                    self.direction = 'positive'
                # yes, and aux run is completed (trigger exit):
                elif self.ycurrent == 0 and self.is_aux_run == True:
                    self.done = True   
                # not final vial (move y, reverse direction). 
                else:
                    # when oriented parallel to axes:
                    if self.parent.orientselect.currentIndex() == 0:
                        if self.is_aux_run == False:
                            self.parent.drive_relative(0, self.ymov)
                            self.ycurrent += 1
                        else:
                            self.parent.drive_relative(0, -self.ymov)
                            self.ycurrent -= 1
                    # when oriented by 1st/last vial in row:
                    else:
                        if self.is_aux_run == False:
                            self.parent.drive_relative(self.row_increment_x, self.row_increment_y)
                            self.ycurrent += 1
                        else:
                            self.parent.drive_relative(-self.row_increment_x, -self.row_increment_y)
                            self.ycurrent -= 1                           
                    self.direction = 'positive'
            # regular single move within row (when no other conditions are fulfilled):
            else:
                # when oriented parallel to axes:
                if self.parent.orientselect.currentIndex() == 0:
                    if self.direction == 'positive':
                        self.parent.drive_relative(self.xmov, 0)
                        self.xcurrent +=1
                    else:
                        self.parent.drive_relative(-self.xmov, 0)
                        self.xcurrent -=1
                # when oriented by 1st/last vial in row:
                else:
                    if self.direction == 'positive':
                        self.parent.drive_relative(self.col_increment_x, self.col_increment_y)
                        self.xcurrent +=1
                    else:
                        self.parent.drive_relative(-self.col_increment_x, -self.col_increment_y)
                        self.xcurrent -=1
        
    def finish(self):
        # deactivate stop button
        self.parent.stop_btn.setEnabled(False)
        self.parent.stop_btn.setVisible(False)
        try:
            self.parent.notebook.tabBarClicked.disconnect(self.parent.handle_tabbar_clicked_nomoves)
        except:
            pass
        self.parent.notebook.tabBarClicked.connect(self.parent.handle_tabbar_clicked)
        self.parent.goto_origin()     # end at A1
        # reset the pointers
        self.direction = 'positive'
        self.firstvial = True
        self.is_aux_run = False
        try:
            self.parent.capture.imageSaved.disconnect()
            self.parent.altcapture.imageSaved.disconnect()
        except:
            pass
        self.parent.status.showMessage('SCAN: finished')
        # send trigger to self-destruct & path name for post-pro
        # wait for camera to save last image:
        self.finished.emit(os.path.join(self.path, 'scan-%s' % (self.timestamp)),self.timestamp)

class MainWindow(qtw.QMainWindow):

    VERSION = '1.0-RC15'
    Simulation_Mode = False
    emergency_stop = qtc.pyqtSignal()

    def __init__(self):
        """MainWindow constructor.
        """
        super().__init__()
        # kill child processes on exit:
        self.setAttribute(qtc.Qt.WA_DeleteOnClose)
        # Main framework
        self.resize(qtc.QSize(1024, 768))
        self.scaleFactor = 1
        self.setWindowTitle('SMoCCscan ' + self.VERSION)
        self.settings = qtc.QSettings('SMoCC','smoccscan')
        self.dist_traveled_x = 0
        self.dist_traveled_y = 0
        self.activecam = 'MAIN'
        self.origin_defined = False
        self.topright_defined = False
        self.drivespeed = 'F' + str(self.settings.value('MA_drivespeed', 1000))
        base_widget = qtw.QWidget()
        base_widget.setLayout(qtw.QHBoxLayout())
        self.notebook = qtw.QTabWidget()
        base_widget.layout().addWidget(self.notebook)
        self.nav_widget = qtw.QWidget()
        self.nav_widget.setLayout(qtw.QVBoxLayout())
        base_widget.layout().addWidget(self.nav_widget)
        self.status = self.statusBar()
        self.setCentralWidget(base_widget)
        self.available_cameras = qtmm.QCameraInfo.availableCameras()
        self.available_ports = comports() 
        # Menus:
        settings_groupbox = qtw.QGroupBox(
            'Settings',
            alignment=qtc.Qt.AlignHCenter
            )
        motion_groupbox = qtw.QGroupBox(
            'Motion Control',
            alignment=qtc.Qt.AlignHCenter
            )
        camera_groupbox = qtw.QGroupBox(
            'Camera Control',
            alignment=qtc.Qt.AlignHCenter
            )
        camera_groupbox.setFixedHeight(250)
        settings_groupbox.setSizePolicy(qtw.QSizePolicy.Minimum,qtw.QSizePolicy.Minimum)
        motion_groupbox.setSizePolicy(qtw.QSizePolicy.Minimum,qtw.QSizePolicy.Minimum)
        camera_groupbox.setSizePolicy(qtw.QSizePolicy.Minimum,qtw.QSizePolicy.Minimum)
        settings_groupbox.setLayout(qtw.QVBoxLayout())
        motion_groupbox.setLayout(qtw.QGridLayout())
        camera_groupbox.setLayout(qtw.QVBoxLayout())
        settings1 = qtw.QFormLayout()
        settings2 = qtw.QGridLayout()
        settings3 = qtw.QFormLayout()
        
        self.dX = qtw.QDoubleSpinBox(
            self,
            value=10,
            maximum=30,
            minimum=0.1,
            suffix=' mm',
            singleStep=1
            )
        self.dY = qtw.QDoubleSpinBox(
            self,
            value=10,
            maximum=30,
            minimum=0.1,
            suffix=' mm',
            singleStep=1
            )
        self.noX = qtw.QSpinBox(
            self,
            value=5,
            maximum=20,
            minimum=1,
            singleStep=1
            )
        self.noY = qtw.QSpinBox(
            self,
            value=5,
            maximum=20,
            minimum=1,
            singleStep=1
            )
        self.altcam_offx = qtw.QDoubleSpinBox(
            self,
            value=45,
            maximum=100,
            minimum=-100,
            suffix=' mm',
            singleStep=1
            )
        self.altcam_offy = qtw.QDoubleSpinBox(
            self,
            value=0,
            maximum=100,
            minimum=-100,
            suffix=' mm',
            singleStep=1
            )
        self.altcam_layout = qtw.QHBoxLayout()
        self.altcam_layout.addWidget(self.altcam_offx)
        self.altcam_layout.addWidget(self.altcam_offy)
        self.noX.setFixedHeight(25)
        self.noY.setFixedHeight(25)
        self.dX.setFixedHeight(25)
        self.dY.setFixedHeight(25)
        self.camselect = qtw.QComboBox()
        self.camselect.addItems([str(i+1)+':'+c.description() for i,c in enumerate(self.available_cameras)])
        self.altcamselect = qtw.QComboBox()
        self.altcamselect.addItem('None / disabled')
        self.altcamselect.addItems([str(i+1)+':'+c.description() for i,c in enumerate(self.available_cameras)])
        self.portselect = qtw.QComboBox()
        self.portselect.addItems([c.description for c in self.available_ports])
        self.dirselect = qtw.QPushButton('directory', self)
        self.as_settings = qtw.QPushButton('Advanced Settings', self)
        self.orientselect = qtw.QComboBox()
        self.orientselect.addItems(['aligned to scan axes', 'calculate from top left / top right vial'])
        self.camselect.setFixedHeight(25)
        self.portselect.setFixedHeight(25)
        self.dirselect.setFixedHeight(25)
        self.as_settings.setFixedHeight(25)
        settings1.addRow('Directory: ', self.dirselect)
        settings1.addRow('', self.as_settings)
        settings1.addRow('Main Camera: ', self.camselect)
        settings1.addRow('Aux Camera: ', self.altcamselect)
        settings1.addRow('Aux x/y offsets: ', self.altcam_layout)
        settings1.addRow('COM Port: ', self.portselect)
        settings1.addRow('Orientation: ', self.orientselect)
        settings2.layout().addWidget(qtw.QLabel('No. Vials X:       '),0,0)
        settings2.layout().addWidget(self.noX,0,1)
        settings2.layout().addWidget(qtw.QLabel('X spacing:'),0,2)
        settings2.layout().addWidget(self.dX,0,3)
        settings2.layout().addWidget(qtw.QLabel('No. Vials Y:       '),1,0)
        settings2.layout().addWidget(self.noY,1,1)
        settings2.layout().addWidget(qtw.QLabel('Y spacing:'),1,2)
        settings2.layout().addWidget(self.dY,1,3)
        settings_groupbox.layout().addLayout(settings1)
        settings_groupbox.layout().addLayout(settings2)
                
        self.deltaM = qtw.QDoubleSpinBox(
            self,
            value=10,
            maximum=100,
            minimum=0.1,
            suffix=' mm',
            singleStep=1
            )
        self.deltaM.setSizePolicy(qtw.QSizePolicy.Fixed,qtw.QSizePolicy.Preferred)        
        self.left = qtw.QPushButton('Left', self)
        self.right = qtw.QPushButton('Right', self)
        self.up = qtw.QPushButton('Up', self)
        self.down = qtw.QPushButton('Down', self)
        self.topleft = qtw.QPushButton('', self)
        self.topright = qtw.QPushButton('', self)
        self.downleft = qtw.QPushButton('', self)
        self.downright = qtw.QPushButton('', self)
        self.setorigin = qtw.QPushButton('Set Top Left', self)
        self.setlast = qtw.QPushButton('Set Top Right', self)
        self.goorigin = qtw.QPushButton('Goto Top Left', self)
        self.goorigin.setEnabled(False)
        motion_groupbox.layout().addWidget(self.left,1,0)
        motion_groupbox.layout().addWidget(self.right,1,2)
        motion_groupbox.layout().addWidget(self.up,0,1)
        motion_groupbox.layout().addWidget(self.down,2,1)
        motion_groupbox.layout().addWidget(self.topleft,0,0)
        motion_groupbox.layout().addWidget(self.topright,0,2)
        motion_groupbox.layout().addWidget(self.downleft,2,0)
        motion_groupbox.layout().addWidget(self.downright,2,2)
        motion_groupbox.layout().addWidget(self.setorigin,3,0)
        motion_groupbox.layout().addWidget(self.setlast,3,1)
        motion_groupbox.layout().addWidget(self.goorigin,3,2)
        motion_groupbox.layout().addWidget(self.deltaM,1,1)

        self.snapshot_btn = qtw.QPushButton('Snapshot (MAIN)')
        self.scan_btn = qtw.QPushButton('Run Scan')
        self.timer_btn = qtw.QPushButton('Run timed Scan')
        self.stop_btn = qtw.QPushButton('>>>>> EMERGENCY STOP <<<<<')
        self.stop_btn.setStyleSheet('background-color: red; color: white')
        camera_groupbox.layout().addWidget(self.snapshot_btn)
        camera_groupbox.layout().addWidget(self.scan_btn)
        camera_groupbox.layout().addWidget(self.timer_btn)
        camera_groupbox.layout().addWidget(self.stop_btn)
        self.scan_btn.setEnabled(False)
        self.timer_btn.setEnabled(False)
        self.stop_btn.setEnabled(False)
        self.stop_btn.setVisible(False)
        
        self.nav_widget.layout().addWidget(settings_groupbox)
        self.nav_widget.layout().addWidget(motion_groupbox)
        self.nav_widget.layout().addWidget(camera_groupbox)
        self.nav_widget.layout().addStretch()

        # Create the viewfinder widget
        self.cvf = qtmmw.QCameraViewfinder()
        self.altcvf = qtmmw.QCameraViewfinder()
        self.select_camera(0)
        self.camera.setViewfinder(self.cvf)
        self.notebook.addTab(self.cvf, 'Live (Main)')

        # create widgets for stitch images
        self.img = mouseableLabel(self)
        self.img.setBackgroundRole(qtg.QPalette.Base)
        self.imgScrollArea = qtw.QScrollArea()
        self.imgScrollArea.setBackgroundRole(qtg.QPalette.Dark)
        self.imgScrollArea.setWidgetResizable(True)
        self.imgScrollArea.setWidget(self.img)
        self.altimg = mouseableLabel(self)
        self.altimg.setBackgroundRole(qtg.QPalette.Base)
        self.altimgScrollArea = qtw.QScrollArea()
        self.altimgScrollArea.setBackgroundRole(qtg.QPalette.Dark)
        self.altimgScrollArea.setWidgetResizable(True)
        self.altimgScrollArea.setWidget(self.altimg)
      
        # populate all inputs from settings and toggle applicable inputs:
        self.getSettings()
        if self.altcamselect.currentIndex() == 0:
            self.altcam_offx.setEnabled(False)
            self.altcam_offy.setEnabled(False)
        else:
            self.altcam_offx.setEnabled(True)
            self.altcam_offy.setEnabled(True)
            self.select_altcamera(self.altcamselect.currentIndex())
            self.notebook.addTab(self.altcvf, 'Live (Aux)')
        if self.orientselect.currentIndex() == 0:
            self.setlast.setEnabled(False)
            self.orientation = 'axes'
        else:
            self.orientation = 'vials'

        # define the work directory & make sure we have at least the smoccscan folder as default dir
        if self.save_path == qtc.QDir.home():
            if not self.save_path.cd('smoccscan'):
                qtc.QDir.home().mkdir('smoccscan')
                self.save_path.cd('smoccscan')

        # signals:
        self.camselect.currentIndexChanged.connect(self.select_camera)
        self.altcamselect.currentIndexChanged.connect(self.select_altcamera)
        self.portselect.currentIndexChanged.connect(self.check_port)
        self.orientselect.currentIndexChanged.connect(self.swap_orient)
        self.dirselect.clicked.connect(self.select_dir)
        self.left.clicked.connect(lambda: self.jog_relative('left'))
        self.right.clicked.connect(lambda: self.jog_relative('right'))
        self.up.clicked.connect(lambda: self.jog_relative('up'))
        self.down.clicked.connect(lambda: self.jog_relative('down'))
        self.topleft.clicked.connect(lambda: self.jog_relative('tl'))
        self.topright.clicked.connect(lambda: self.jog_relative('tr'))
        self.downleft.clicked.connect(lambda: self.jog_relative('dl'))
        self.downright.clicked.connect(lambda: self.jog_relative('dr'))
        self.setorigin.clicked.connect(self.define_origin) 
        self.setlast.clicked.connect(self.define_topright) 
        self.goorigin.clicked.connect(self.goto_origin)
        self.snapshot_btn.clicked.connect(lambda: self.snapshot(self.activecam))
        self.snapshot_btn.clicked.connect(self.setSettings)
        self.scan_btn.clicked.connect(self.fullscan)
        self.scan_btn.clicked.connect(self.setSettings)
        self.timer_btn.clicked.connect(self.timedscan)
        self.timer_btn.clicked.connect(self.setSettings)
        self.as_settings.clicked.connect(self.setAsSettings)
        self.stop_btn.clicked.connect(self.emergency_stop.emit)
        self.stop_btn.clicked.connect(self.cleanuptimedscan)
        self.stop_btn.clicked.connect(lambda: self.status.showMessage('SCAN: interrupted by user'))
        self.img.clicked_left.connect(self.zoomIn)
        self.altimg.clicked_left.connect(self.zoomIn)
        self.altimg.clicked_right.connect(self.zoomOut)
        self.img.clicked_right.connect(self.zoomOut)
        self.notebook.tabBarClicked.connect(self.handle_tabbar_clicked)
        self.show()
        self.check_port()

    def keyPressEvent(self, event):
        """Handles motion control via numpad keys (Num Lock must be on).
        """
        if type(event) == qtg.QKeyEvent:
            if event.key() == qtc.Qt.Key_1:
                self.jog_relative('dl')
            elif event.key() == qtc.Qt.Key_2:
                self.jog_relative('down')
            elif event.key() == qtc.Qt.Key_3:
                self.jog_relative('dr')
            elif event.key() == qtc.Qt.Key_4:
                self.jog_relative('left')
            elif event.key() == qtc.Qt.Key_6:
                self.jog_relative('right')
            elif event.key() == qtc.Qt.Key_7:
                self.jog_relative('tl')
            elif event.key() == qtc.Qt.Key_8:
                self.jog_relative('up')
            elif event.key() == qtc.Qt.Key_9:
                self.jog_relative('tr')
            elif event.key() == qtc.Qt.Key_Plus:
                self.zoomIn()
            elif event.key() == qtc.Qt.Key_Minus:
                self.zoomOut()                
  
    def select_dir(self):
        self.save_path = qtc.QDir(qtw.QFileDialog.getExistingDirectory(self, 'Select Folder'))
        self.dirselect.setText(self.save_path.absolutePath())

    def select_camera(self, i):
        self.camera = qtmm.QCamera(self.available_cameras[i])
        self.camera.setViewfinder(self.cvf)
        self.camera.setCaptureMode(qtmm.QCamera.CaptureStillImage)
        self.camera.error.connect(lambda: self.return_error(self.camera.errorString()))
        self.camera.start()
        # Configure capture
        self.capture = qtmm.QCameraImageCapture(self.camera)
        self.capture.error.connect(lambda i, e, s: self.return_error(s))
        self.current_camera_name = self.available_cameras[i].description()
        self.save_seq = 0
        settings = self.capture.encodingSettings()
        supportedResolutions, _ = self.capture.supportedResolutions()
        self.status.showMessage("Supported resolutions:" + str([str(r.width())+"x"+str(r.height()) for r in supportedResolutions]))
        # just default to highest available resolution for now:
        settings.setResolution(supportedResolutions[-1].width(), supportedResolutions[-1].height())
        settings.setQuality(qtmm.QMultimedia.VeryHighQuality)
        self.capture.setEncodingSettings(settings)

    def select_altcamera(self, i):
        if i == 0:
            self.altcam_offx.setEnabled(False)
            self.altcam_offy.setEnabled(False)
            try:
                self.notebook.removeTab(1)
                if self.mainscantab:
                    self.mainscantab -= 1
                if self.auxscantab:
                    self.auxscantab -= 1
            except:
                pass            
        else:
            self.altcam_offx.setEnabled(True)
            self.altcam_offy.setEnabled(True)
            self.altcamera = qtmm.QCamera(self.available_cameras[i-1])
            self.altcamera.setViewfinder(self.altcvf)
            self.altcamera.setCaptureMode(qtmm.QCamera.CaptureStillImage)
            self.altcamera.error.connect(lambda: self.return_error(self.altcamera.errorString()))
            self.altcamera.start()
            # Configure capture
            self.altcapture = qtmm.QCameraImageCapture(self.altcamera)
            self.altcapture.error.connect(lambda i, e, s: self.return_error(s))
            self.current_altcamera_name = self.available_cameras[i].description()
            settings = self.altcapture.encodingSettings()
            supportedResolutions, _ = self.altcapture.supportedResolutions()
            self.status.showMessage("Supported resolutions:" + str([str(r.width())+"x"+str(r.height()) for r in supportedResolutions]))
            # just default to highest available resolution for now:
            settings.setResolution(supportedResolutions[-1].width(), supportedResolutions[-1].height())
            settings.setQuality(qtmm.QMultimedia.VeryHighQuality)
            self.altcapture.setEncodingSettings(settings)
            try:
                self.altcamera.setViewfinder(self.altcvf)
                self.notebook.insertTab(1, self.altcvf, 'Live (Aux)')
                if self.mainscantab:
                    self.mainscantab += 1
                if self.auxscantab:
                    self.auxscantab += 1
            except:
                pass

    def swap_orient(self, i):
        if i == 0:
            self.setlast.setEnabled(False)
            self.orientation = 'axes'
        else:
            self.setlast.setEnabled(True)
            self.orientation = 'vials'

    def return_error(self, error):
        print(error)

    def handle_tabbar_clicked(self, index):
        ''' for now, the only thing we want to do when clicking tabs is driving
        between main and aux cam settings. So we only do anything when auxcam is
        present and activated. In this case the index for main = 0, aux = 1 
        '''
        # do nothing if no altcam:
        if self.altcamselect.currentIndex() == 0:
            pass
        # do this when altcam is present:
        else:
            # nothing changed:
            if index == 0 and self.activecam == 'MAIN':
                pass
            # nothing changed:
            elif index == 1 and self.activecam == 'AUX':
                pass
            # swap from aux to main:
            elif index == 0 and self.activecam == 'AUX':
                self.activecam = 'MAIN'
                self.drive_relative(self.altcam_offx.value(), self.altcam_offy.value())
                self.snapshot_btn.setText('Snapshot (MAIN)')
            # swap from main to aux:    
            elif index == 1 and self.activecam == 'MAIN':
                self.activecam = 'AUX'
                self.drive_relative(-self.altcam_offx.value(), -self.altcam_offy.value())
                self.snapshot_btn.setText('Snapshot (AUX)')
                
    def handle_tabbar_clicked_nomoves(self, index):
        ''' passive version only swaps self.activecam (used e.g. for returns to origin)
        '''
        # do nothing if no altcam:
        if self.altcamselect.currentIndex() == 0:
            pass
        # do this when altcam is present:
        else:
            # nothing changed:
            if index == 0 and self.activecam == 'MAIN':
                pass
            # nothing changed:
            elif index == 1 and self.activecam == 'AUX':
                pass
            # swap from aux to main:
            elif index == 0 and self.activecam == 'AUX':
                self.activecam = 'MAIN'
                self.snapshot_btn.setText('Snapshot (MAIN)')
            # swap from main to aux:    
            elif index == 1 and self.activecam == 'MAIN':
                self.activecam = 'AUX'
                self.snapshot_btn.setText('Snapshot (AUX)')

    def check_port(self):
        if not self.Simulation_Mode:
            try:
                self.con.close()
            except:
                pass
            try:
                port = self.available_ports[self.portselect.currentIndex()].device
                con = serial.Serial(port, 115200, timeout=5)
                string = con.read(10)        # read up to ten bytes (timeout)
                if b'\r\nGrbl 1.1' in string:
                    self.status.showMessage('Grbl 1.1 ready on ' + port)
                    self.con = con
                    self.drivespeed = 'F' + str(self.settings.value('MA_drivespeed', 1000))
                    cmd = '$X\r\n'         # reset errors
                    self.con.write(cmd.encode())
                    if self.settings.value('MA_homing') == 'true':                             # homing (if active)
                        self.status.showMessage('HOMING&INIT: starting')
                        cmd = '$H\r\n'                        
                        self.con.write(cmd.encode())
                        if self.settings.value('MA_initialpos') == 'true':                         # drive to starting position (only if homing active)
                            self.drive_relative(str(self.settings.value('MA_initial_x')), str(self.settings.value('MA_initial_y')))
                    return True
                else:
                    self.status.showMessage('no XY stage listening on ' + port)
                    con.close()
            except:
                print('Error probing COM port.')
        else:
            con = 'simulation'

    def define_origin(self):
        if self.activecam == 'AUX':
            self.dist_traveled_x = -self.altcam_offx.value()
            self.dist_traveled_y = -self.altcam_offy.value()
        else:
            self.dist_traveled_x = 0
            self.dist_traveled_y = 0
        self.goorigin.setEnabled(True)
        self.origin_defined = True
        if self.orientselect.currentIndex() == 0:
            self.scan_btn.setEnabled(True)
            self.timer_btn.setEnabled(True)
        elif self.topright_defined:
            self.scan_btn.setEnabled(True)
            self.timer_btn.setEnabled(True)
        if not self.Simulation_Mode:
            self.status.showMessage('new origin defined (= top left vial)')
        else:
            self.status.showMessage('new origin defined (= top left vial) - SIMULATION MODE')
                    
    def define_topright(self):
        if self.dist_traveled_x == 0:
            qtw.QMessageBox.about(self, 'Coordinate Sanity Check', 'Caution: Top right vial cannot sit on origin, move x and/or y axis and try again.')
            return
        elif self.activecam == 'MAIN' and self.dist_traveled_x**2 + self.dist_traveled_y**2 < ((self.noX.value()-1) * self.dX.value())**2-(self.dX.value())**2:
            qtw.QMessageBox.about(self, 'Coordinate Sanity Check', 'Caution: Number of vials & dX do not match with top right vial definition (travelled distance too small). Move x axis and try again.')
            return
        elif self.activecam == 'MAIN' and self.dist_traveled_x**2 + self.dist_traveled_y**2 > ((self.noX.value()-1) * self.dX.value())**2+(self.dX.value())**2:
            qtw.QMessageBox.about(self, 'Coordinate Sanity Check', 'Caution: Number of vials & dX do not match with top right vial definition (travelled distance too long). Did you skip a vial? Move x axis and try again.')
            return
        elif self.activecam == 'AUX' and (self.dist_traveled_x+self.altcam_offx.value())**2 + (self.dist_traveled_y+self.altcam_offy.value())**2 < ((self.noX.value()-1) * self.dX.value())**2-(self.dX.value())**2:
            qtw.QMessageBox.about(self, 'Coordinate Sanity Check', 'Caution: Number of vials & dX do not match with top right vial definition (travelled distance too small). Move x axis and try again.')
            return
        elif self.activecam == 'AUX' and (self.dist_traveled_x+self.altcam_offx.value())**2 + (self.dist_traveled_y+self.altcam_offy.value())**2 > ((self.noX.value()-1) * self.dX.value())**2+(self.dX.value())**2:
            qtw.QMessageBox.about(self, 'Coordinate Sanity Check', 'Caution: Number of vials & dX do not match with top right vial definition (travelled distance too long). Did you skip a vial? Move x axis and try again.')
            return
        else:
            if self.activecam == 'AUX':
                self.rotation = (atan2(self.dist_traveled_y+self.altcam_offy.value(),self.dist_traveled_x+self.altcam_offx.value()))
            else:
                self.rotation = (atan2(self.dist_traveled_y,self.dist_traveled_x))
            self.topright_defined = True
            if self.origin_defined:
                self.scan_btn.setEnabled(True)
                self.timer_btn.setEnabled(True)
            if not self.Simulation_Mode:
                self.status.showMessage('new orientation defined (rotation angle: '+str(round(degrees(self.rotation),3))+'°)')
            else:
                self.status.showMessage('new orientation defined (rotation angle: '+str(round(degrees(self.rotation),3))+'°) - SIMULATION MODE')
    
    def goto_origin(self):
        try:
            if not self.Simulation_Mode:
                if self.activecam == 'AUX':
                    self.drive_relative(-self.dist_traveled_x-self.altcam_offx.value(),-self.dist_traveled_y-self.altcam_offy.value())
                else:
                    self.drive_relative(-self.dist_traveled_x,-self.dist_traveled_y)
                dist = sqrt(self.dist_traveled_x**2 + self.dist_traveled_y**2)
            else:
                dist = 10
            # min 2 seconds delay, use QEventloop to make it non-blocking:
            loop = qtc.QEventLoop()
            qtc.QTimer.singleShot(max(int(dist*100),2000), loop.quit)
            loop.exec_()
        except:
            pass
            #self.status.showMessage('connection error')    

    def drive_relative(self, distx, disty):
        """Worker function for actual programmed moves.
           This is main-loop blocking, do not use for prolonged moves!
        """
        try:
            if self.con:
                self.drivespeed = 'F' + str(self.settings.value('MA_drivespeed', 1000))            
                cmd = 'G91 G1 '         # relative mode, linear move
                cmd += 'X' + str(distx) + ' ' + 'Y' + str(disty) + ' '
                cmd += self.drivespeed + ' '         # drive speed
                self.status.showMessage('moving: ' +cmd)
                cmd += '\r\n'
                if not self.Simulation_Mode:
                    self.con.write(cmd.encode())      # DANGER! Always uncomment real move request for testing!
                self.dist_traveled_x += distx
                self.dist_traveled_y += disty
        except:
            pass
            #self.status.showMessage('connection error')    

    def jog_relative(self, direction):
        # possibly implement real-time jogging mode here (i.e. no timeouts)
        dist = self.deltaM.value()
        try:
            self.drivespeed = 'F' + str(self.settings.value('MA_drivespeed', 1000))
            cmd = 'G91 G1 '         # relative mode, linear move
            if direction == 'up':
                self.dist_traveled_y -= dist
                cmd += 'Y-' + str(dist) + ' '       # axis & dist
            elif direction == 'down':
                self.dist_traveled_y += dist
                cmd += 'Y' + str(dist) + ' '        # axis & dist
            elif direction == 'left':
                self.dist_traveled_x -= dist
                cmd += 'X-' + str(dist) + ' '       # axis & dist
            elif direction == 'tl':
                self.dist_traveled_x -= dist
                self.dist_traveled_y -= dist
                cmd += 'X-' + str(dist) + ' ' + 'Y-' + str(dist) + ' '
            elif direction == 'tr':
                self.dist_traveled_x += dist
                self.dist_traveled_y -= dist
                cmd += 'X' + str(dist) + ' ' +  'Y-' + str(dist) + ' '                 
            elif direction == 'dl':
                self.dist_traveled_x -= dist
                self.dist_traveled_y += dist
                cmd += 'X-' + str(dist) + ' ' +  'Y'  + str(dist) + ' '                
            elif direction == 'dr':
                self.dist_traveled_x += dist
                self.dist_traveled_y += dist
                cmd += 'X' + str(dist) + ' ' +  'Y'  + str(dist) + ' '                 
            else:
                self.dist_traveled_x += dist
                cmd += 'X' + str(dist) + ' '        # axis & dist
            cmd += self.drivespeed + ' '         # drive speed
            if not self.Simulation_Mode:
                self.status.showMessage('move '+direction+' (relative): '+cmd)
            else:
                self.status.showMessage('move '+direction+' (relative): '+cmd+' - SIMULATION MODE')
            cmd += '\r\n'           # terminal linefeed
            if not self.Simulation_Mode:
                self.con.write(cmd.encode())      # DANGER! Always uncomment real move request for testing!
        except:
            pass
            #self.status.showMessage('connection error')

    def snapshot(self,camera):
        if not self.save_path.cd('snapshots'):
            self.save_path.mkdir('snapshots')
            self.save_path.cd('snapshots')
        timestamp = time.strftime('%d%b%Y-%H_%M_%S')
        if camera == 'MAIN':
            file = os.path.join(self.save_path.absolutePath(), 'main-%s.jpg' % (
                timestamp
                ))
            self.capture.capture(file)
        elif camera == 'AUX':
            file = os.path.join(self.save_path.absolutePath(), 'aux-%s.jpg' % (
                timestamp
                ))
            self.altcapture.capture(file)
        self.save_path.cdUp()
        self.status.showMessage('Image ' + file + ' captured')
        self.save_seq += 1

    def pause_loop(self, delaytime):
        """uses QEventloop to make delay non-blocking
        """
        #print('pause triggered, ' + str(delaytime))
        loop = qtc.QEventLoop()
        qtc.QTimer.singleShot(delaytime, loop.quit)
        loop.exec_()

    def scanshot(self, file, camera):
        if camera == 'main':
            self.capture.capture(file)
        elif camera == 'aux':
            self.altcapture.capture(file)

    def fullscan(self):
        self.scan = FullScan(self)
        self.scan_thread = qtc.QThread()
        self.disableControls()
        self.scan.finished.connect(self.scan_thread.quit)
        self.scan.finished.connect(self.stitch)
        self.scan.finished.connect(lambda: self.analyze('last'))
        self.scan.finished.connect(self.enableControls)
        self.scan.snapshot.connect(self.scanshot)
        self.scan.moveToThread(self.scan_thread)
        self.scan_thread.start()
        self.scan.run()
        
    def timedscan(self):
        self.timedscanwindow = TimedScanWindow(self)
        self.timedscanwindow.abort.connect(self.emergency_stop.emit)

    def cleanuptimedscan(self):
        try:
            self.timedscanwindow.on_close()
        except:
            pass
        
    def setAsSettings(self):
        self.as_settingswindow = AsSettingsWindow(self)

    def stitch(self, path, timestamp):
        outfile = os.path.basename(path) + '.png'
        outfile = outfile.replace('scan','main')
        images_main = [f for f in os.listdir(path) if re.match(r'main-[A-Z][0-9]+.*\.jpg', f)]
        # adjust parameters as needed: resize factor, color, font point size
        outfile_full = os.path.join(self.save_path.absolutePath(), outfile)
        result = self.merge_images(path, timestamp, images_main, 'main')   
        result.save(outfile_full, 'PNG', 100)
        self.mainpix = result
        self.img.setPixmap(self.mainpix)
        try:
            images_aux = [f for f in os.listdir(path) if re.match(r'aux-[A-Z][0-9]+.*\.jpg', f)]
            if images_aux:
                outfile = os.path.basename(path) + '.png'
                outfile = outfile.replace('scan','aux')
                outfile_full = os.path.join(self.save_path.absolutePath(), outfile)
                result = self.merge_images(path, timestamp, images_aux, 'aux')
                result.save(outfile_full, 'PNG', 100)
                self.auxpix = result
                self.altimg.setPixmap(self.auxpix)
        except:
            print('Error with stitch image list (aux).')
        # delete old tabs in reverse order ;-)
        try:
            self.notebook.removeTab(self.auxscantab)
        except:
            pass
        try:
            self.notebook.removeTab(self.mainscantab)
        except:
            pass
        # store the tab indices for later:
        self.mainscantab = self.notebook.addTab(self.imgScrollArea, 'Last Scan (Main)')
        if images_aux:
            self.auxscantab = self.notebook.addTab(self.altimgScrollArea, 'Last Scan (Aux)')

    def merge_reprocess(self):
        dialog = qtw.QMessageBox.question(self, 'Reprocess Auto-Stitch',
        'Reprocess all scans in\n'+self.save_path.absolutePath()+ ' ?\nWARNING: Single original snapshots '
        +'will stay untouched, but any existing stitched images will be overwritten using current settings.',
        qtw.QMessageBox.Ok | qtw.QMessageBox.Cancel, qtw.QMessageBox.Ok)
        if dialog == qtw.QMessageBox.Ok:
            # find folders:
            folders = [ f.path for f in os.scandir(self.save_path.absolutePath()) if f.is_dir() ]
            try:
                cnt = 0
                for i in folders:
                    try:
                        if os.path.basename(i).startswith('scan-'):
                            cnt += 1
                            timestamp = (os.path.basename(i).replace('scan-',''))
                            self.stitch(i,timestamp)                 
                    except:
                        pass
                self.status.showMessage('Auto-Stitch: reprocessing finished ('+str(cnt)+' scans)')    
            except:
                self.status.showMessage('Auto-Stitch: reprocessing error')
        else:
            self.status.showMessage('Auto-Stitch: reprocessing cancelled')   

    def analyze_reprocess(self):
        # find folders:
        folders = [ f.path for f in os.scandir(self.save_path.absolutePath()) if f.is_dir() ]
        # purge existing table file:
        try:
            os.remove(os.path.join(self.save_path.absolutePath(), 'analysis.txt'))   
        except:
            pass
        try:
            cnt = 0
            for i in folders:
                try:
                    if os.path.basename(i).startswith('scan-'):
                        cnt += 1
                except:
                    pass
            self.analyze('full')
            self.status.showMessage('Analyze: reprocessing finished ('+str(cnt)+' scans)')    
        except:
            self.status.showMessage('Analyze: reprocessing error')
            
    def analyze(self, type):
        #print('analysis finished, type: ',type)
        folders = [ f.path for f in os.scandir(self.save_path.absolutePath()) if f.is_dir() ]
        scanlist = []
        for i in folders:
            try:
                if os.path.basename(i).startswith('scan-'):
                    scanlist.append(i)
            except:
                pass
        outfile = os.path.join(self.save_path.absolutePath(), 'analysis.txt')
        dir = self.save_path.absolutePath()
        # detect available vials from first scan:
        dir0 = os.path.join(self.save_path.absolutePath(), scanlist[0])
        files = [ f.path for f in os.scandir(dir0) ]
        header = []
        for i in files:
            str1 = os.path.basename(i).replace('main-','').replace('aux-','').replace('.jpg','')
            if os.path.basename(i).startswith('main-'):
                str2 = 'MAIN'
            else:
                str2 = 'AUX'
            header.append([str1,str2])
        # write header lines if not already present:
        headerstr = ''
        if not os.path.isfile(outfile):
            headerstr = '                    '       # space for timedate
            for i in header:
                headerstr += i[0]+'    '              # vial label + separator
            headerstr += '\n'
            headerstr += '                    '  # space for timedate
            for i in header:
                if i[1] == 'MAIN':
                    headerstr += i[1]+'  '           # vial cam + separator
                else:
                    headerstr += i[1]+'   '  
        with open(outfile, 'a') as f:
            f.write(headerstr)
            if type == 'full':
                rng1 = 1
                rng2 = len(scanlist)
            else:
                rng1 = len(scanlist)-1
                rng2 = len(scanlist)
            for i in range(rng1,rng2):
                timedatestamp = os.path.basename(scanlist[i]).replace('scan-','')
                datestamp = timedatestamp.split('-')[0]
                timestamp = timedatestamp.split('-')[1].replace('_',':')
                f.write('\n'+datestamp+' '+timestamp+'  ')
                for j in files:
                    path1 = os.path.join(dir,scanlist[i])
                    path2 = os.path.join(dir,scanlist[i-1])
                    file = os.path.basename(j)
                    file1 = os.path.join(path1,file)
                    file2 = os.path.join(path2,file)
                    #print(file1+' - '+file2)
                    ### calculations here (not done yet so just using some random numbers for testing):
                    result = '{:.2f}'.format(round(random.uniform(0, 1),2))
                    ### end calculations
                    f.write(result+'  ')

    def merge_images(self, path, timestamp, files, cam):
        """Merge images into one, displayed in rows/columns
        as set in dropdowns. Returns PIL image object.
        Note: Use Image.open always in a "with" construct to make sure image object is
        properly killed. Image.close() in PIL does not work correctly and produces a massive
        memory leak. Also Image.save() apparently produces problems.
        """
        # get settings for stitch:
        if cam == 'main':
            resize_factor = float(self.settings.value('AS_main_resizefactor', '0.5'))
            refpix = int(self.settings.value('AS_main_refpix', 35))
            refmm = float(self.settings.value('AS_main_reflength', 0.4))
            spacing = float(self.settings.value('AS_main_spacing', 0.2))
            camstring = 'MAIN'
        else:
            resize_factor = float(self.settings.value('AS_aux_resizefactor', '0.5'))
            refpix = int(self.settings.value('AS_aux_refpix', 35))
            refmm = float(self.settings.value('AS_aux_reflength', 0.4))
            spacing = float(self.settings.value('AS_aux_spacing', 0.2))
            camstring = 'AUX'
        default_anno = self.settings.value('AS_defaultanno', True, type=bool)  
        custom_anno = self.settings.value('AS_customanno', False, type=bool)
        showgrid = self.settings.value('AS_showgrid', False, type=bool)
        custom_text = self.settings.value('AS_notes', 'my Notes')
        # grab colors in correct format for PIL:
        gridcol = self.settings.value('AS_gridcolor', qtg.QColor('#00FF00'))
        bordercol = self.settings.value('AS_bordercolor', qtg.QColor('#FFFFFF'))
        vialcol = self.settings.value('AS_vialcolor', qtg.QColor('#FFFFFF'))
        annocol = self.settings.value('AS_annotationcolor', qtg.QColor('#FFFF00'))
        fnt = qtg.QFont()
        fnt.fromString(self.settings.value('AS_font', qtg.QFont("Helvetica", 10).toString()))
        fnt_metrics = qtg.QFontMetrics(fnt)
        text_size = fnt.pointSize()
        # handle single image dimensions:
        reference = qtg.QPixmap(os.path.join(path, files[0]))
        width = int(round(reference.width() * resize_factor))
        height = int(round(reference.height() * resize_factor))
        # extract number of columns & rows, also removes necessity to sort the list:
        images = []
        rows = 0
        cols = 0
        for i in files:
            letter = re.findall('([A-Z])', i)
            row = ord(letter[0])-64
            col = int(re.search(r'\d+', i).group(0))
            if row > rows:
                rows = row
            if col > cols:
                cols = col
            # append image position to new list:
            images.append((os.path.join(path,i),col,row))
        # generate container image (accounting for 2 pixels border each):
        result_width = (width+2) * cols + 2
        result_height = (height+2) * rows + 2
        if self.settings.value('AS_defaultanno', type=bool) == True\
        or self.settings.value('AS_customanno', type=bool) == True\
        or self.settings.value('AS_showgrid', type=bool) == True:        
            string_height = fnt_metrics.height()
        else:
            string_height = 0
        # work on the stitched canvas:
        result = qtg.QPixmap(result_width, result_height+string_height)
        result.fill(bordercol)
        rpainter = qtg.QPainter(result)
        rpainter.setFont(fnt)
        rpen = qtg.QPen()
        rpen.setColor(annocol)
        rpainter.setPen(rpen)
        # Annotation handling:
        if self.settings.value('AS_defaultanno', type=bool) == True:
            string = timestamp.replace('_',':').replace('-','  ')
            rpainter.drawText(6, result_height+string_height-8, string)
            rpainter.drawText(result_width-fnt_metrics.width(camstring)-6, result_height+string_height-8, camstring)
        if self.settings.value('AS_customanno', type=bool) == True:
            string_width = fnt_metrics.width(custom_text)
            rpainter.drawText(int(result_width/2-string_width/2),result_height+string_height-8 ,custom_text)
        if self.settings.value('AS_showgrid', type=bool) == True:
            # calculate spacing on image for drawing function:
            imgspacing = int(round(resize_factor*spacing*refpix/refmm))
            # how many lines to draw in each quadrant:
            noH = int(round(0.5*height/imgspacing))
            noV = int(round(0.5*width/imgspacing))
            #print('pixels:',imgspacing,'refpix:',refpix,' refmm:',refmm,' factor:',resize_factor,'spacing',spacing)
            string2 = 'grid:'+str(round(spacing,1))+'mm' 
            if self.settings.value('AS_defaultanno', type=bool) == True:
                string2_width = fnt_metrics.width(camstring)+fnt_metrics.width(string2)+fnt_metrics.width('  ')            
            else:
                string2_width = fnt_metrics.width(string2)
            rpainter.drawText(result_width-string2_width-6, result_height+string_height-8, string2)
        # Image stitching:
        for i in images:
            f = qtg.QPixmap(i[0])
            f = f.scaled(width,height, qtc.Qt.IgnoreAspectRatio, qtc.Qt.SmoothTransformation)
            fpainter = qtg.QPainter(f)
            # additive mode seems to work best to blend overlay with image pixels:
            fpainter.setCompositionMode(qtg.QPainter.CompositionMode_Plus)
            # transparent overlay layer:
            f_edit = qtg.QPixmap(width, height)
            f_edit.fill(qtg.QColor(qtc.Qt.transparent))
            ipainter = qtg.QPainter(f_edit)
            ipainter.setFont(fnt)
            ipen = qtg.QPen()
            # Grid handling:
            if self.settings.value('AS_showgrid', type=bool) == True:
                ipen.setColor(gridcol)   # gridline color
                ipen.setWidth(1)         # gridline width
                ipainter.setPen(ipen)
                ipainter.setCompositionMode(qtg.QPainter.CompositionMode_Source)
                # vertical:
                for v in range(noV):
                    ipainter.drawLine(qtc.QPoint(int(width/2)+int(imgspacing/2)+v*imgspacing, 3), qtc.QPoint(int(width/2)+int(imgspacing/2)+v*imgspacing, height-3))
                    ipainter.drawLine(qtc.QPoint(int(width/2)-int(imgspacing/2)-v*imgspacing, 3), qtc.QPoint(int(width/2)-int(imgspacing/2)-v*imgspacing, height-3))
                # horizontal:
                for h in range(noH):
                    ipainter.drawLine(qtc.QPoint(3, int(height/2)+int(imgspacing/2)+h*imgspacing), qtc.QPoint(width-3, int(height/2)+int(imgspacing/2)+h*imgspacing))
                    ipainter.drawLine(qtc.QPoint(3, int(height/2)-int(imgspacing/2)-h*imgspacing), qtc.QPoint(width-3, int(height/2)-int(imgspacing/2)-h*imgspacing))
            ipen.setColor(vialcol)
            ipainter.setPen(ipen)
            # extract small image labels:
            string = os.path.splitext(i[0])[0]
            string = string.rsplit('-', 1)[1]
            ipainter.drawText(width-fnt_metrics.width(string)-4, height-4, string)
            x = i[1] - 1
            y = i[2] - 1
            fpainter.drawPixmap(0,0,f_edit)
            rpainter.drawPixmap(x*(width+2)+2,y*(height+2)+2,f)  # include the 2pix border
            ipainter.end()
            fpainter.end()
        rpainter.end()
        return result
        
    def getSettings(self):
        """Recover settings, convert from str back to QObj
        where necessary.
        """
        self.noX.setValue(int(self.settings.value('noX', 5)))
        self.noY.setValue(int(self.settings.value('noY', 5)))
        self.dX.setValue(float(self.settings.value('dX', 10)))
        self.dY.setValue(float(self.settings.value('dY', 10)))
        self.altcam_offx.setValue(float(self.settings.value('auxCamOffsetX', -45)))
        self.altcam_offy.setValue(float(self.settings.value('auxCamOffsetY', 0)))
        self.deltaM.setValue(float(self.settings.value('deltaM', 10)))
        self.save_path = qtc.QDir(self.settings.value('savePath', qtc.QDir.home().absolutePath()))
        self.dirselect.setText(self.save_path.absolutePath())
        self.interval_minutes = (int(self.settings.value('interval', 55)))
        # retranslate unique camera&port strings back to selection index:
        for i, c in enumerate(self.available_cameras):
            if self.settings.value('camera', 'none') == c.deviceName():
                self.camselect.setCurrentIndex(i)
                self.select_camera(i)
        for i, c in enumerate(self.available_cameras):
            if self.settings.value('auxcamera', 'none') == c.deviceName():
                self.altcamselect.setCurrentIndex(i)
                self.select_altcamera(i)
            else:
                self.select_altcamera(0)
        for i, c in enumerate(self.available_ports):
            if self.settings.value('comPort', 'none') == c.device:
                self.portselect.setCurrentIndex(i)
                self.check_port()
        self.orientselect.setCurrentIndex(int(self.settings.value('orientation', 0)))
        
    def setSettings(self):
        """Save settings whenever one of the camera buttons
        are clicked.
        """
        self.settings.setValue('noX', self.noX.value())
        self.settings.setValue('noY', self.noY.value())
        self.settings.setValue('dX', self.dX.value())
        self.settings.setValue('dY', self.dY.value())
        self.settings.setValue('auxCamOffsetX', self.altcam_offx.value())
        self.settings.setValue('auxCamOffsetY', self.altcam_offy.value())
        self.settings.setValue('deltaM', self.deltaM.value())
        self.settings.setValue('interval', self.interval_minutes)
        self.settings.setValue('savePath', self.save_path.absolutePath())   # store as string
        # store camera&port as unique device string:
        try:
            camera = self.available_cameras[self.camselect.currentIndex()].deviceName()
            self.settings.setValue('camera', camera)
        except:
            pass
        try:
            auxcamera = self.available_cameras[self.altcamselect.currentIndex()].deviceName()
            self.settings.setValue('auxcamera', auxcamera)
        except:
            self.settings.setValue('auxcamera', 'none')
        try:
            comport = self.available_ports[self.portselect.currentIndex()].device
            self.settings.setValue('comPort', comport)
        except:
            pass
        self.settings.setValue('orientation', self.orientselect.currentIndex())

    def disableControls(self):
        """Disable controls while scanning.
        """
        for button in self.findChildren(qtw.QPushButton):
            if not button.text() == '>>>>> EMERGENCY STOP <<<<<':
                button.setEnabled(False)
            else:
                button.setEnabled(True)
        for sb in self.findChildren(qtw.QSpinBox):
            sb.setEnabled(False)
        for dsb in self.findChildren(qtw.QDoubleSpinBox):
            dsb.setEnabled(False)
        for cb in self.findChildren(qtw.QComboBox):
            cb.setEnabled(False)
        
    def enableControls(self):
        """Enable controls after scanning finished.
        """
        for button in self.findChildren(qtw.QPushButton):
            if not button.text() == '>>>>> EMERGENCY STOP <<<<<':
                button.setEnabled(True)
            else:
                button.setEnabled(False)        
        for sb in self.findChildren(qtw.QSpinBox):
            sb.setEnabled(True)
        for dsb in self.findChildren(qtw.QDoubleSpinBox):
            dsb.setEnabled(True)
        for cb in self.findChildren(qtw.QComboBox):
            cb.setEnabled(True)
        if self.altcamselect.currentIndex() == 0:
            self.altcam_offx.setEnabled(False)
            self.altcam_offy.setEnabled(False)
        else:
            self.altcam_offx.setEnabled(True)
            self.altcam_offy.setEnabled(True)
        if self.orientselect.currentIndex() == 0:
            self.setlast.setEnabled(False)
        else:
            self.setlast.setEnabled(True)

    def scaleImage(self, factor):
        self.scaleFactor *= factor
        try:
            self.img.setPixmap(self.mainpix.scaled(self.scaleFactor * self.mainpix.size()))
            self.adjustScrollBar(self.imgScrollArea.horizontalScrollBar(), factor)
            self.adjustScrollBar(self.imgScrollArea.verticalScrollBar(), factor)
        except:
            pass
        try:
            self.altimg.setPixmap(self.auxpix.scaled(self.scaleFactor * self.auxpix.size()))
            self.adjustScrollBar(self.altimgScrollArea.horizontalScrollBar(), factor)
            self.adjustScrollBar(self.altimgScrollArea.verticalScrollBar(), factor)
        except:
            pass

    def adjustScrollBar(self, scrollBar, factor):
        scrollBar.setValue(int(factor * scrollBar.value() + ((factor - 1) * scrollBar.pageStep() / 2)))

    def zoomIn(self):
        self.scaleImage(1.25)

    def zoomOut(self):
        self.scaleImage(0.8)

    def closeEvent(self, event):
        self.setSettings()


if __name__ == '__main__':
    app = qtw.QApplication(sys.argv)
    mw = MainWindow()
    sys.exit(app.exec())
