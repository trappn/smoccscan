# SMoCCscan

GUI to control a scanning microscope and produce image series along a grid.

PyQt5 GUI script to control a simple GRBL1.1-driven XY stage. It can take images at intervals along
a rectangular grid (such as from a USB microscope attached to the stage). The grid can be defined in two ways:
Either it is assumed to be parallel to the motion axes and defined by the top left position, or the top left
and top right corner positions can be defined for the program to compensate for any tilt. Settings are persistent.
Images are labeled A1, A2, A3, ... B1, B2, etc. (rows = letters, columns = numbers). Scans and snapshots are time-stamped.
A second camera is now supported ("Aux camera"), e.g. for zoomed images. "Last Scan" preview tabs can be zoomed (left/right
mouse buttons or +/- keys). A mosaic/stitching tool is now implemented, which runs automatically after each scan,
with several customization options (including a scalable grid overlay).
<br>

Requires: PyQt5, PyQt5-multimedia, pyserial

I take no responsibility whatsoever for any damage arising from use of this software.
In your own interest, drive your motors safely.
<br>
My original hardware was a modified variant of the excellent 4xiDraw: https://www.thingiverse.com/thing:1444216 (commercial version: https://shop.evilmadscientist.com/846), running on an Arduino MEGA RAMPS1.4 3D printer control board with GRBL1.1f firmware (modified for RAMPS pinout and CoreXY geometry). My new system is based on Openbuilds ACRO 55 hardware and a Makerbase MKS DLC V2.1 controller, running Openbuilds precompiled 2-axis GRBL1.1g (SMoCCscan RC13 and higher optionally supports hardware endstops). This runs a lot smoother and with no focusing issues.
<br><br>


Control flow:
1. select camera, COM port, target directory & orientation mode in Settings
2. [optional: set auxiliary camera and desired its x&y offsets, check on aux live preview]
3. define your grid (# rows / columns and spacing)
4. drive stage to top left starting position using the motion control buttons,
verify with camera
5. press "Set Top Left" to define origin (I don't use end switches to keep it simple)
 [optional: if orientation mode "calculate from top left / top right vial" is active, also drive to top right position
 and press "Set Top Right"] 
6. scans are now unlocked. Run Scan = one Scan, Run timed Scan = Scan series
(this will open a clock&settings window)
<br>

![GUI overview image](/images/gui_preview_1.png "SMoCCscan GUI window")
![GUI lastscan image](/images/gui_preview_2.png "SMoCCscan GUI window")
![GUI stitch settings image](/images/gui_preview_3.png "SMoCCscan GUI window")
![GUI stitch settings image](/images/hardware_setup2.jpg "Hardware Setup")
