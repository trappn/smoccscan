$fn = 80;
noX = 5;
dX = 20;
noY = 5;
dY = 20;
dBore = 12.0; // standard HPLC vial including 0.2mm tolerance (tight fit 11.8, loose fit 12.0)
h = 10;

// note: the upright subgrids are offset 1mm along Y for all grid types to preserve
// interoperability!

module vial_upright() {
  difference() {
    translate([-dX/2,-dY/2,0]) cube([dX, dY, h]);
    // direction marker for subgrid:
    translate([-0.5,-0.5,h-0.4]) cube([1, dY, 1]);
    translate([-dX/2,-dY/2-1,0.4]) cylinder(d=dBore, h=h+1);
    translate([dX/2,-dY/2-1,0.4]) cylinder(d=dBore, h=h+1);
    translate([-dX/2,dY/2-1,0.4]) cylinder(d=dBore, h=h+1);  
    translate([dX/2,dY/2-1,0.4]) cylinder(d=dBore, h=h+1);
    translate([-dX/2,-dY/2-1,-0.4]) cylinder(d=dBore-0.5, h=1);  
    translate([dX/2,-dY/2-1,-0.4]) cylinder(d=dBore-0.5, h=1);  
    translate([-dX/2,dY/2-1,-0.4]) cylinder(d=dBore-0.5, h=1);  
    translate([dX/2,dY/2-1,-0.4]) cylinder(d=dBore-0.5, h=1); 
    translate([-dX/2,-dY/2,-0.2]) cylinder(d=dX/2, h=h+1);
    translate([dX/2,-dY/2,-0.2]) cylinder(d=dX/2, h=h+1);
    translate([-dX/2,dY/2,-0.2]) cylinder(d=dX/2, h=h+1);  
    translate([dX/2,dY/2,-0.2]) cylinder(d=dX/2, h=h+1);
    translate([0,0,0.4]) cylinder(d=dBore, h=h);
    translate([0,0,-0.4]) cylinder(d=dBore-0.5, h=1);  
  }  
}

module vial_45() {
//color("red")  translate([0,-dY/4,h/2-1]) rotate([-45,0,0]) cylinder(d=11.6, h=33);
  difference() {
    translate([-dX/2,-dY/2,0]) cube([dX, dY, h]);
    // direction marker for subgrid:
    //translate([-0.5,-0.5,h-0.4]) cube([1, dY, 1]);
    translate([-dX/2,-dY/2-1,0.4]) cylinder(d=dBore, h=h+1);
    translate([dX/2,-dY/2-1,0.4]) cylinder(d=dBore, h=h+1);
    translate([-dX/2,dY/2-1,0.4]) cylinder(d=dBore, h=h+1);  
    translate([dX/2,dY/2-1,0.4]) cylinder(d=dBore, h=h+1);
    translate([-dX/2,-dY/2-1,-0.4]) cylinder(d=dBore-0.5, h=1);  
    translate([dX/2,-dY/2-1,-0.4]) cylinder(d=dBore-0.5, h=1);  
    translate([-dX/2,dY/2-1,-0.4]) cylinder(d=dBore-0.5, h=1);  
    translate([dX/2,dY/2-1,-0.4]) cylinder(d=dBore-0.5, h=1);
    translate([0,-dY,h+dBore/2-1]) rotate([-90,0,0]) cylinder(d=dBore, h=2*dY);  
    hull() {
      translate([0,-dY/4+1,h/2-1]) rotate([-45,0,0]) cylinder(d=dBore, h=2*h);
      translate([0,-dY/4+1,-0.4]) cylinder(d=dBore-1, h=2);
    }
  }  
}
     


module vial_20() {
//color("red")  translate([0,-dY/4+1,h/2-3]) rotate([-20,0,0]) cylinder(d=11.6, h=33);
  difference() {
    translate([-dX/2,-dY/2,0]) cube([dX, dY, h]);
    // direction marker for subgrid:
    translate([-0.5,-0.5,h-0.4]) cube([1, dY, 1]);
    translate([-dX/2,-dY/2-1,0.4]) cylinder(d=dBore, h=h+1);
    translate([dX/2,-dY/2-1,0.4]) cylinder(d=dBore, h=h+1);
    translate([-dX/2,dY/2-1,0.4]) cylinder(d=dBore, h=h+1);  
    translate([dX/2,dY/2-1,0.4]) cylinder(d=dBore, h=h+1);
    translate([-dX/2,-dY/2-1,-0.4]) cylinder(d=dBore-0.5, h=1);  
    translate([dX/2,-dY/2-1,-0.4]) cylinder(d=dBore-0.5, h=1);  
    translate([-dX/2,dY/2-1,-0.4]) cylinder(d=dBore-0.5, h=1);  
    translate([dX/2,dY/2-1,-0.4]) cylinder(d=dBore-0.5, h=1); 
    hull() {
      translate([0,-dY/4+1,h/2-3]) rotate([-20,0,0]) cylinder(d=dBore, h=2*h);
      translate([0,-dY/4+1,-0.4]) cylinder(d=dBore-1, h=2);
    }  
  }  
}

module vial_30() {
//color("red")  translate([0,-dY/4+1,h/2-2]) rotate([-30,0,0]) cylinder(d=11.6, h=33);
  difference() {
    translate([-dX/2,-dY/2,0]) cube([dX, dY, h]);
    // direction marker for subgrid:
    translate([-0.5,-0.5,h-0.4]) cube([1, dY, 1]);
    translate([-dX/2,-dY/2-1,0.4]) cylinder(d=dBore, h=h+1);
    translate([dX/2,-dY/2-1,0.4]) cylinder(d=dBore, h=h+1);
    translate([-dX/2,dY/2-1,0.4]) cylinder(d=dBore, h=h+1);  
    translate([dX/2,dY/2-1,0.4]) cylinder(d=dBore, h=h+1);
    translate([-dX/2,-dY/2-1,-0.4]) cylinder(d=dBore-0.5, h=1);  
    translate([dX/2,-dY/2-1,-0.4]) cylinder(d=dBore-0.5, h=1);  
    translate([-dX/2,dY/2-1,-0.4]) cylinder(d=dBore-0.5, h=1);  
    translate([dX/2,dY/2-1,-0.4]) cylinder(d=dBore-0.5, h=1);  
    hull() {
      translate([0,-dY/4+1,h/2-2]) rotate([-30,0,0]) cylinder(d=dBore, h=2*h);
      translate([0,-dY/4+1,-0.4]) cylinder(d=dBore-1, h=2);
    }  
  }  
}

module grid_upright() {
  for (a = [0 : noX-1])
    for (b = [0 : noY-1])
    translate([dX*a,dY*b,0])  vial_upright();
}

module grid_45() {
  for (a = [0 : noX-1])
    for (b = [0 : noY-1])
    translate([dX*a,dY*b,0])  vial_45();
}

module grid_20() {
  for (a = [0 : noX-1])
    for (b = [0 : noY-1])
    translate([dX*a,dY*b,0])  vial_20();
}

module grid_30() {
  for (a = [0 : noX-1])
    for (b = [0 : noY-1])
    translate([dX*a,dY*b,0])  vial_30();
}


// WHAT TO RENDER:

// Single vials (for testing):
//translate([0,0,0]) vial_upright();
//translate([20,0,0]) vial_45();
//translate([40,0,0]) vial_20();

// Grids (20° seems to be best compromise):
//grid_upright();
//grid_45();
//grid_30();
grid_20();